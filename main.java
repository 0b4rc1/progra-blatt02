public class main {
  public static void main(String []args) {
    int d = SimpleIO.getInt("Bitte geben Sie die Tageskomponente des Startdatums ein.");
    int m = SimpleIO.getInt("Bitte geben Sie die Monatskomponente des Startdatums ein.");
    int y = SimpleIO.getInt("Bitte geben Sie die Jahreskomponente des Startdatums ein.");
    int t = SimpleIO.getInt("Bitte geben Sie die Anzahl an zu addierenden Tagen ein");
    int[] NUM_DAYS_PER_MONTH = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    d += t;
    int days_this_month = NUM_DAYS_PER_MONTH[m - 1];

    while ((d / (days_this_month + 1)) > 0) {
      d -= days_this_month;

      if (m % NUM_DAYS_PER_MONTH.length == 0) {
        y++;
        m = 1;
      } else {
        m++;
      }
      days_this_month = NUM_DAYS_PER_MONTH[m - 1];
    }
    System.out.println("Das Datum ist, nachdem "+t+" Tage addiert wurden, der " + d + "." + 	m + "." + y+".");
  }
}
